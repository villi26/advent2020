/// @description Insert description here
// You can write your code in this editor
if (mouse_over() && mouse_check_button_pressed(mb_left)) {
	var grid = day6_grid();
	str = string(day6_count_any(grid)) + "\n" + string(day6_count_every(grid));
	
	for (var i = 0; i < ds_grid_width(grid); i++) {
		ds_map_destroy(grid[# i, 0]);
	}
	
	ds_grid_destroy(grid);
}