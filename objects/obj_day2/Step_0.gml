/// @description Insert description here
// You can write your code in this editor
if (mouse_over() && mouse_check_button_pressed(mb_left)) {
	scr_day2();
	var infile = open_file();
		
	var count1 = 0;
	var count2 = 0;

	while (!file_text_eof(infile)) {
		var line = file_text_readln(infile);
		var splits = string_split(line, " ");
		
		var minimum, maximum;
		var rule = string_split(splits[| 0], "-");
		minimum = int64(rule[| 0]);
		maximum = int64(rule[| 1]);
		
		var char = string_char_at(splits[| 1], 1);
		
		if (clamp(string_count(char, splits[| 2]), minimum, maximum) == string_count(char, splits[| 2])) {
			count1++;
		}
		
		if (string_char_at(splits[| 2], minimum) == char ^^ string_char_at(splits[| 2], maximum) == char) {
			count2++;
		}
		
		ds_list_destroy(splits);
	}
	
	file_text_close(infile);

	str = string(count1) + "\n" + string(count2);
}