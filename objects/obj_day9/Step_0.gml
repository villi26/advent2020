/// @description Insert description here
// You can write your code in this editor

if (mouse_over() && mouse_check_button_pressed(mb_left)) {
	var values = day9_values();
	var invalid = day9_part1(values);
	str = string(invalid) + "\n" + string(day9_part2(values, invalid));
}