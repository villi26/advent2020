/// @description Insert description here
// You can write your code in this editor
if (mouse_over() && mouse_check_button_pressed(mb_left)) {
	var grid = day3_grid();
	str = string(day3_count(grid, 3, 1));
	var product = day3_count(grid, 1, 1) * day3_count(grid, 3, 1) * day3_count(grid, 5, 1)
			* day3_count(grid, 7, 1) * day3_count(grid, 1, 2);
	str += "\n" + string(product);
	ds_grid_destroy(grid);
}