/// @description Insert description here
// You can write your code in this editor
if (mouse_over() && mouse_check_button_pressed(mb_left)) {
	var list = day4_maps();
	str = string(day4_count(list));
	str += "\n" + string(day4_count_full(list));
	
	for (var i = 0; i < ds_list_size(list); i++) {
		ds_map_destroy(list[| i]);
	}
	
	ds_list_destroy(list);
}