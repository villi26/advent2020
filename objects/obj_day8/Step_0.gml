/// @description Insert description here
// You can write your code in this editor
if (mouse_over() && mouse_check_button_pressed(mb_left)) {
	var instructions = day8_instructions();
	str = string(day8_run(instructions));
	day8_reset(instructions);
	instructions = day8_fix(instructions);
	str += "\n" + string(day8_run(instructions));
	
	ds_grid_destroy(instructions);
}