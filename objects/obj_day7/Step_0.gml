/// @description Insert description here
// You can write your code in this editor

if (mouse_over() && mouse_check_button_pressed(mb_left)) {
	var map = day7_map();
	str = string(day7_gold_count(map)) + "\n" + string(day7_part2(map, "shiny gold"));
	
	ds_map_destroy(map);
}