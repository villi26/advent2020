// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function real_string_char_at(str, index) {
	return string_char_at(str, index + 1);
}