// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function ds_grids_equal(grid1, grid2) {
	if (ds_grid_width(grid1) != ds_grid_width(grid2) || ds_grid_height(grid1) != ds_grid_height(grid2)) {
		return false;	
	}
	
	for (var i = 0; i < ds_grid_width(grid1); i++) {
		for (var j = 0; j < ds_grid_height(grid1); j++) {
			if (grid1[# i, j] != grid2[# i, j]) {
				return false;
			}
		}
	}
	
	return true;
}