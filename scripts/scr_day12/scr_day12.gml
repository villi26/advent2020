// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function day12_instructions() {
	var file = open_file();
	var grid = ds_grid_create(0, 2);
	var line = "";
	var rows = 0;
	
	while (!file_text_eof(file)) {
		line = file_text_readln(file);
		rows++;
		ds_grid_resize(grid, rows, 2);
		
		grid[# rows - 1, 0] = string_char_at(line, 1);
		grid[# rows - 1, 1] = int64(string_copy(line, 2, string_length(line) - 1));
	}
	
	return grid;
}

enum ShipDir {
	East = 0,
	North = 1,
	West = 2,
	South = 3,
}

function day12_part1(instructions) {
	// I'm using cartesian coordinates, so east is positive x, west is negative x,
	// north is positive y and south is negative y.
	var new_instructions = ds_grid_create(0, 0);
	ds_grid_copy(new_instructions, instructions);
	var xpos = 0;
	var ypos = 0;
	var dir = ShipDir.East;
	var forward = ["E", "N", "W", "S"];
	
	for (var i = 0; i < ds_grid_width(new_instructions); i++) {
		if (new_instructions[# i, 0] == "F") {
			new_instructions[# i, 0] = forward[dir];
		}
		
		switch (new_instructions[# i, 0]) {
			case "E":
				xpos += new_instructions[# i, 1];
				break;
			case "N":
				ypos += new_instructions[# i, 1];
				break;
			case "W":
				xpos -= new_instructions[# i, 1];
				break;
			case "S":
				ypos -= new_instructions[# i, 1];
				break;
			case "L":
				dir += new_instructions[# i, 1] / 90;
				dir %= 4;
				break;
			case "R":
				dir -= new_instructions[# i, 1] / 90;
				dir %= 4;
				while (dir < 0) {
					dir += 4;	
				}
				break;
		}
	}
	
	return abs(xpos) + abs(ypos);
}

function day12_part2(instructions) {
	var new_instructions = ds_grid_create(0, 0);
	ds_grid_copy(new_instructions, instructions);
	var pos = [0, 0];
	var wp = [10, 1];
	
	for (var i = 0; i < ds_grid_width(new_instructions); i++) {
		switch (new_instructions[# i, 0]) {
			case "F":
				repeat (new_instructions[# i, 1]) {
					pos[0] += wp[0];
					pos[1] += wp[1];
				}
				
				break;
			case "E":
				wp[0] += new_instructions[# i, 1];
				break;
			case "N":
				wp[1] += new_instructions[# i, 1];
				break;
			case "W":
				wp[0] -= new_instructions[# i, 1];
				break;
			case "S":
				wp[1] -= new_instructions[# i, 1];
				break;
			case "L":
			case "R":
				day11_rotate(wp, new_instructions[# i, 0], new_instructions[# i, 1]);
				break;
		}
	}
	
	return abs(pos[0]) + abs(pos[1]);
}

function day11_rotate(wp, dir, angle) {
	repeat (angle / 90) {
		switch (dir) {
			case "L":
				var temp = wp[0];
				wp[0] = -wp[1];
				wp[1] = temp;
				break;
			case "R":
				var temp = wp[1];
				wp[1] = -wp[0];
				wp[0] = temp;
				break;
			default:
				throw("Either this didn't happen or you're really bad at this");
		}
	}
}