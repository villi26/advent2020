// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function ds_grid_to_string(grid) {
	var str = "";
	
	for (var i = 0; i < ds_grid_height(grid); i++) {		
		for (var j = 0; j < ds_grid_width(grid); j++) {
			str += grid[# j, i];
		}
		
		if (i != ds_grid_height(grid) - 1) {
			str += "\n";
		}
	}
	
	return str;
}