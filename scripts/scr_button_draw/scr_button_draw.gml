// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function button_draw(colour, str) {
	if (mouse_over()) {
		draw_sprite(sprite_index, 1, x, y);
	} else {
		draw_sprite(sprite_index, 0, x, y);
	}

	draw_set_color(colour);
	draw_set_halign(fa_center);
	draw_text(x, y + 60, str);
}