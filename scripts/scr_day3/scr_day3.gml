// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function day3_grid() {
	var file = open_file();
	// Create an empty grid
	var grid = ds_grid_create(0,0);
	
	// Get the first line and resize the width of the grid to fit it, then add it to the grid
	var rows = 1;
	var line = file_text_readln(file);
	ds_grid_resize(grid, string_length(line) - 1, 1);
	add_line_to_grid(grid, line, 0);
	
	while (!file_text_eof(file)) {
		line = file_text_readln(file);
		rows++;
		ds_grid_resize(grid, string_length(line) - 1, rows);
		add_line_to_grid(grid, line, rows - 1);
	}
	
	file_text_close(file);
	
	return grid;
}

// Function to add a string to a ds_grid, one character per cell, at the specified row.
// Assumes the grid's width >= the width of the string.
function add_line_to_grid(grid, line, row) {
	for (var i = 1; i < string_length(line); i++) {
		grid[# i - 1, row] = string_char_at(line, i);
	}
}

function day3_count(grid, right, down) {
	var xpos = 0;
	var ypos = 0;
	var width = ds_grid_width(grid);
	var height = ds_grid_height(grid);
	var count = 0;
	
	do {
		if (grid[# xpos, ypos] == "#") {
			count++;
		}
		
		xpos += right;
		ypos += down;
		xpos %= width;
	} until (ypos >= height);
	
	return count;
}