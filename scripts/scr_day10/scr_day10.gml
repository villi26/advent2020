// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function day10_array() {
	var file = open_file();
	var array = [0];
	
	while (!file_text_eof(file)) {
		array_push(array, int64(string_replace(file_text_readln(file), "\n", "")));
	}
	
	array_push(array, array_max(array) + 3);
	
	return array;
}

function day10_part1(array) {
	array_sort(array, true);
	var threes = 0;
	var ones = 0;
	
	for (var i = 1; i < array_length(array); i++) {
		switch(array[i] - array[i - 1]) {
			case 1:
				ones++;
				break;
			case 3:
				threes++;
				break;
			default:
				return -1;
		}
	}
	
	return ones * threes;
}

function day10_count(array) {
	var map = day10_map(array);
	var target = array_max(array);
	var memo = ds_map_create();
	
	return day10_paths_from(map, 0, target, memo);
}

function day10_paths_from(map, entry, target, memo) {
	if (entry == target) {
		return 1;
	}
	
	if (is_in_array(ds_map_keys_to_array(memo), entry)) {
		return memo[? entry];	
	}
	
	var values = map[? entry];
	var res = 0;
	
	for (var i = 0; i < array_length(values); i++) {
		res += day10_paths_from(map, values[i], target, memo);
	}
	
	memo[? entry] = res;
	
	return res;
}

function day10_map(array) {
	var map = ds_map_create();
	
	for (var i = 0; i < array_length(array); i++) {
		map[? array[i]] = [];
		
		for (var j = 1; j <= 3; j++) {
			if (is_in_array(array, array[i] + j)) {
				array_push(map[? array[i]], array[i] + j);
			}
		}
	}
	
	return map;
}