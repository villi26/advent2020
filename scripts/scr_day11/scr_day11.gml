// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function day11_grid() {
	var file = open_file();
	var line = string_replace(file_text_readln(file), "\n", "");
	var grid = ds_grid_create(string_length(line), 1);
	day11_add_line_to_grid(grid, line, 0);
	var rows = 1;
	
	while (!file_text_eof(file)) {
		line = string_replace(file_text_readln(file), "\n", "");
		rows++;
		ds_grid_resize(grid, string_length(line), rows);
		day11_add_line_to_grid(grid, line, rows - 1);
	}
	
	file_text_close(file);
	
	return grid;
}

function day11_add_line_to_grid(grid, line, row) {
	for (var i = 1; i <= string_length(line); i++) {
		grid[# i - 1, row] = string_char_at(line, i);
	}
}

function day11_part1(grid) {
	var new_grid = ds_grid_create(0,0);
	ds_grid_copy(new_grid, grid);
	
	while (true) {
		if (day11_evolve(new_grid)) {
			break;	
		}
	}
	
	var res = day11_count(new_grid);
	ds_grid_destroy(new_grid);
	return res;
}

function day11_part2(grid) {
	var new_grid = ds_grid_create(0,0);
	ds_grid_copy(new_grid, grid);
	
	while (true) {
		if (day11_evolve_part2(new_grid)) {
			break;	
		}
	}
	
	var res = day11_count(new_grid);
	ds_grid_destroy(new_grid);
	return res;
}

function day11_evolve(grid) {
	var new_grid = ds_grid_create(0, 0);
	ds_grid_copy(new_grid, grid);
	
	for (var i = 0; i < ds_grid_width(grid); i++) {
		for (var j = 0; j < ds_grid_height(grid); j++) {
			if (grid[# i, j] == "L" && day11_adjacent(grid, i, j) == 0) {
				new_grid[# i, j] = "#";
			} else if (grid[# i, j] == "#" && day11_adjacent(grid, i, j) >= 4) {
				new_grid[# i, j] = "L";
			}
		}
	}
	
	var res = ds_grids_equal(grid, new_grid);
	
	ds_grid_copy(grid, new_grid);
	ds_grid_destroy(new_grid);
	return res;
}

function day11_evolve_part2(grid) {
	var new_grid = ds_grid_create(0, 0);
	ds_grid_copy(new_grid, grid);
	
	for (var i = 0; i < ds_grid_width(grid); i++) {
		for (var j = 0; j < ds_grid_height(grid); j++) {
			if (grid[# i, j] == "L" && day11_visible(grid, i, j) == 0) {
				new_grid[# i, j] = "#";
			} else if (grid[# i, j] == "#" && day11_visible(grid, i, j) >= 5) {
				new_grid[# i, j] = "L";
			}
		}
	}
	
	var res = ds_grids_equal(grid, new_grid);
	
	ds_grid_copy(grid, new_grid);
	ds_grid_destroy(new_grid);
	return res;
}

function day11_adjacent(grid, xpos, ypos) {
	var count = 0;
	
	for (var i = -1; i <= 1; i++) {
		for (var j = -1; j <= 1; j++) {
			if (i == 0 && j == 0) {
				continue;
			}
			
			if (xpos + i < 0 || ypos + j < 0 || xpos + i >= ds_grid_width(grid) 
					|| ypos + j >= ds_grid_height(grid)) {
				continue;
			}
			
			if (grid[# xpos + i, ypos + j] == "#") {
				count++;	
			}
		}
	}
	
	return count;
}

function day11_visible(grid, xpos, ypos) {
	var count = 0;
	
	for (var i = -1; i <= 1; i++) {
		for (var j = -1; j <= 1; j++) {
			if (i == 0 && j == 0) {
				continue;
			}
			
			var xcheck = xpos;
			var ycheck = ypos;
			
			while (true) {
				xcheck += i;
				ycheck += j;
				
				if (xcheck < 0 || ycheck < 0 || xcheck >= ds_grid_width(grid) 
					|| ycheck >= ds_grid_height(grid)) {
					break;
				}
				
				if (grid[# xcheck, ycheck] == "L") {
					break;	
				} else if (grid[# xcheck, ycheck] == "#") {
					count++;
					break;
				}
			}
		}
	}
	
	return count;
}

function day11_count(grid) {
	var count = 0;
	
	for (var i = 0; i < ds_grid_width(grid); i++) {
		for (var j = 0; j < ds_grid_height(grid); j++) {
			if (grid[# i, j] == "#") {
				count++;
			}
		}
	}
	
	return count;
}