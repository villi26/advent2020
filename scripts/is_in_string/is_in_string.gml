// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function is_in_string(str, char) {
	for (var i = 1; i <= string_length(str); i++) {
		if (string_char_at(str, i) == char) {
			return true;
		}
	}
	
	return false;
}