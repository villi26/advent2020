// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function array_sum_range(array, start, stop){
	var sum = 0;
	
	for (var i = start; i < stop; i++) {
		sum += array[i];
	}
	
	return sum;
}