// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function day1_part1(list) {
	var size = ds_list_size(list);
	var num1, num2;
	
	for (var i = 0; i < size; i++) {
		for (var j = 0; j < size; j++) {
			if (list[| i] + list[| j] == 2020) {
				num1 = list[| i];
				num2 = list[| j];
				return string(num1 * num2);
			}
		}
	}
	
	return "Uh oh";
}

function day1_part2(list) {
	var size = ds_list_size(list);
	var num1, num2, num3;
	
	for (var i = 0; i < size; i++) {
		for (var j = 0; j < size; j++) {
			for (var k = 0; k < size; k++) {
				if (list[| i] + list[| j] + list[| k] == 2020) {
					num1 = list[| i];
					num2 = list[| j];
					num3 = list[| k];
					return string(num1 * num2 * num3);
				}
			}
		}
	}
	
	return "Uh oh";
}

function day1_list() {
	var file = open_file();
	var list = ds_list_create();
	
	while (!file_text_eof(file)) {
		ds_list_add(list, int64(string_replace(file_text_readln(file), "\n", "")));
	}
	
	return list;
}