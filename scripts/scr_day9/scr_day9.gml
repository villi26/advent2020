// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function day9_values() {
	var file = open_file();
	var values = [];
	
	while (!file_text_eof(file)) {
		var val = int64(string_replace(file_text_readln(file), "\n", ""));
		array_push(values, val);
	}
	
	file_text_close(file);
	return values;
}

function day9_part1(values) {
	for (var v = 25; v < array_length(values); v++) {
		var allowed_values = [];
		
		for (var i = v - 25; i < v; i++) {
			for (var j = v - 25; j < v; j++) {
				if (i == j) {
					continue;
				}
			
				array_push(allowed_values, values[i] + values[j]);
			}
		}
		
		if (!is_in_array(allowed_values, values[v])) {
			return values[v];	
		}
	}
	
	return -1;
}

function day9_part2(values, invalid) {
	show_debug_message(array_sum([1, 2, 3, 4, 5]));
	for (var len = 2; len <= array_length(values); len++) {
		for (var v = 0; v < array_length(values) - len; v++) {
			var numbers = [];
			array_copy(numbers, 0, values, v, len);
		
			if (array_sum(numbers) == invalid) {
				return array_max(numbers) + array_min(numbers);
			}
		}
	}
	
	return -1;
}