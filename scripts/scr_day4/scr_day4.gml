// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function day4_maps() {
	var file = open_file();
	var maps = ds_list_create();
	var index = 0;
	
	while (!file_text_eof(file)) {
		var line = "";
		maps[| index] = ds_map_create();
		
		while (line != "\n" && !file_text_eof(file)) {
			line = file_text_readln(file);
			var splits = string_split(string_replace(line, "\n", ""), " ");
			
			for (var i = 0; i < ds_list_size(splits); i++) {
				var pair = string_split(splits[| i], ":");
				maps[| index][? pair[| 0]] = pair[| 1];
			}
		} 
		
		index++;
	}
	
	file_text_close(file);
	
	return maps;
}

function day4_count(list) {
	var count = 0;
	
	for (var i = 0; i < ds_list_size(list); i++) {
		if (day4_valid(list[| i])) {
			count++;
		}
	}
	
	return count;
}

function day4_valid(map) {
	var requirements = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"];
	
	for (var i = 0; i < array_length(requirements); i++) {
		if (is_undefined(map[? requirements[i]])) {
			return false;
		}
	}
	
	return true;
}

function day4_count_full(list) {
	var count = 0;
	
	for (var i = 0; i < ds_list_size(list); i++) {
		if (day4_valid_full(list[| i])) {
			count++;
		}
	}
	
	return count;
}

function day4_valid_full(map) {
	// The map must contain all the keys required
	if (!day4_valid(map)) {
		return false;
	}
	
	// Check the dates on the passport
	// Check the birth year is 4 digits between 1920 and 2002
	// and the issue year is 4 digits between 2010 and 2020
	// and the expiration year is between 2020 and 2030.
	var byr = int64(map[? "byr"]);
	var iyr = int64(map[? "iyr"]);
	var eyr = int64(map[? "eyr"]);
	
	if (byr < 1920 || byr > 2002 || iyr < 2010 || iyr > 2020 || eyr < 2020 || eyr > 2030) {
		return false;
	}
	
	// Check the height - the last two chars must be "in" or "cm" and then the preceeding
	// must be a number in range (determined by the units)
	var hgtstr = map[? "hgt"];
	var height_units = string_copy(hgtstr, string_length(hgtstr) - 1, 2);
	
	if (height_units != "cm" && height_units != "in") {
		return false;
	}
	
	var hgt = int64(string_copy(hgtstr, 1, string_length(hgtstr) - 2));
	
	if ((height_units == "cm" && (hgt < 150 || hgt > 193)) 
			|| (height_units == "in" && (hgt < 59 || hgt > 76))) {
		return false;
	}
	
	// Check the hair colour is a valid hex 6-digit hex number following a #
	var hcl = map[? "hcl"];
	
	if (string_length(hcl) != 7 || string_char_at(hcl, 1) != "#") {
		return false;
	}
	
	if (!day4_valid_hex(string_copy(hcl, 2, 6))) {
		return false;
	}
	
	// Chech the eye colour is one of the allowed colours
	var valid_ecl = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"];
	
	if (!is_in_array(valid_ecl, map[? "ecl"])) {
		return false;
	}
	
	// Check the passport id is a valid number and is 9 digits long (including 0s)
	var pid = map[? "pid"];
	
	if (string_length(pid) != 9 || !is_string_numeric(pid)) {
		return false;
	}
	 
	return true;
}

function day4_valid_hex(str) {
	var valid = "0123456789abcdef";
	
	for (var i = 1; i <= 6; i++) {
		if (!is_in_string(valid, string_char_at(str, i))) {
			return false;
		}
	}
	
	return true;
}

function is_string_numeric(str) {
	var valid = "0123456789";
	
	for (var i = 1; i <= 9; i++) {
		if (!is_in_string(valid, string_char_at(str, i))) {
			return false;
		}
	}
	
	return true;
}