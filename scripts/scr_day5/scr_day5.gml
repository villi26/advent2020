// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function day5_list() {
	var file = open_file();
	var list = ds_list_create();
	var index = 0;
	
	while (!file_text_eof(file)) {
		list[| index] = string_replace(file_text_readln(file), "\n", "");
		index++;
	}
	
	file_text_close(file);
	
	return list;
}

function day5_ids() {
	var list = day5_list();
	var ids;
	
	for (var i = 0; i < ds_list_size(list); i++) {
		ids[i] = day5_row(list[| i]) * 8 + day5_column(list[| i]);
	}
	
	return ids;
}

function day5_seat(ids) {
	array_sort(ids, true);
	
	var current = ids[0];
	
	for (var i = 1; i < array_length(ids); i++) {
		if (ids[i] - current != 1) {
			return current + 1;
		}
		
		current = ids[i];
	}
	
	return -1;
}

function day5_row(str) {
	return day5_row_helper(string_copy(str, 1, string_length(str) - 3), 0, 127);
}

function day5_row_helper(str, low, high) {
	if (str == "") {
		return low;	
	}
	
	var newlow = low;
	var newhigh = high;
	var diff = (high + 1 - low) / 2;
	
	if (string_char_at(str, 1) == "F") {
		newhigh -= diff;
	} else if (string_char_at(str, 1) == "B") {
		newlow += diff;
	} else return -1;
	
	return day5_row_helper(string_copy(str, 2, string_length(str) - 1), newlow, newhigh);
}

function day5_column(str) {
	return day5_column_helper(string_copy(str, string_length(str) - 2, 3), 0, 7);
}

function day5_column_helper(str, low, high) {
	if (str == "") {
		return low;	
	}
	
	var newlow = low;
	var newhigh = high;
	var diff = (high + 1 - low) / 2;
	
	if (string_char_at(str, 1) == "L") {
		newhigh -= diff;
	} else if (string_char_at(str, 1) == "R") {
		newlow += diff;
	} else return -1;
	
	return day5_column_helper(string_copy(str, 2, string_length(str) - 1), newlow, newhigh);
}