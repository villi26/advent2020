// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function ds_list_to_string(list) {
	var str = "[";
		
	for (var i = 0; i < ds_list_size(list); i++) {
		str += string(list[| i]);
		
		if (i != ds_list_size(list) - 1) {
			str += ", ";
		}
	}
	
	str += "]";
	return str;
}