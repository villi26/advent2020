// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function day6_grid() {
	var file = open_file();
	var grid = ds_grid_create(1, 2);
	var index = 0;
	grid[# index, 0] = ds_map_create();
	grid[# index, 1] = 0;
	
	var line = "";
	
	while (!file_text_eof(file)) {
		line = string_replace(file_text_readln(file), "\n", "");
		
		if (line == "") {
			index++;
			ds_grid_resize(grid, index + 1, 2);
			
			grid[# index, 0] = ds_map_create();
			grid[# index, 1] = 0;
			continue;
		}
		
		for (var i = 1; i <= string_length(line); i++) {
			if (is_undefined(grid[# index, 0][? string_char_at(line, i)])) {
				grid[# index, 0][? string_char_at(line, i)] = 1;	
			} else {
				grid[# index, 0][? string_char_at(line, i)]++;	
			}
		}
		
		grid[# index, 1]++;
	}
	
	return grid;
}

function day6_count_any(grid) {
	var count = 0;
	
	for (var i = 0; i < ds_grid_width(grid); i++) {
		count += ds_map_size(grid[# i, 0]);	
	}
	
	return count;
}

function day6_count_every(grid) {
	var count = 0;
	
	for (var i = 0; i < ds_grid_width(grid); i++) {
		var keys = ds_map_keys_to_array(grid[# i, 0]);
		
		for (var j = 0; j < array_length(keys); j++) {
			if (grid[# i, 0][? keys[j]] == grid[# i, 1]) {
				count++;
			}
		}
	}
	
	return count;
}

/* // I just wrote this one for fun
function day6_list_to_string(list) {
	var str = "[ ";
	
	for (var i = 0; i < ds_list_size(list); i++) {
		str += ds_map_to_string(list[| i]);
		
		if (i != ds_list_size(list) - 1) {
			str += ", ";	
		}
	}
	
	str += " ]";
	return str;
}

function ds_map_to_string(map) {
	var str = "{ ";
		
	var keys = ds_map_keys_to_array(map);
	for (var i = 0; i < array_length(keys); i++) {
		str += string(keys[i]) + " : " + string(map[? keys[i]]);
		
		if (i != array_length(keys) - 1) {
			str += ", "
		}
	}
	
	str += " }"
	return str;
}*/