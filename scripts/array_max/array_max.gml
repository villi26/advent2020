// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function array_max(array) {
	if (array_length(array) == 0) {
		return undefined;
	}
	
	var maximum = array[0];
	
	for (var i = 1; i < array_length(array); i++) {
		if (array[i] > maximum)	{
			maximum = array[i];
		}
	}
	
	return maximum;
}