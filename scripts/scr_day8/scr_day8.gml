// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function day8_instructions() {
	var file = open_file();
	var instructions = ds_grid_create(0, 3);
	var width = 0;
	
	while (!file_text_eof(file)) {
		width++;
		var line = string_replace(file_text_readln(file), "\n", "");
		var splits = string_split(line, " ");
		ds_grid_resize(instructions, width, 3);
		
		instructions[# width - 1, 0] = splits[| 0];
		instructions[# width - 1, 1] = int64(splits[| 1]);
		// The third row will store how many times the instruction has been run.
		instructions[# width - 1, 2] = 0;
	}
	
	return instructions;
}

function day8_reset(instructions) {
	for (var i = 0; i < ds_grid_width(instructions); i++) {
		instructions[# i, 2] = 0;	
	}
}

function day8_fix(instructions) {
	for (var i = 0; i <= ds_grid_width(instructions); i++) {
		var new_instructions = ds_grid_create(ds_grid_width(instructions), 3);
		ds_grid_copy(new_instructions, instructions);
		
		if (instructions[# i, 0] == "nop") {
			new_instructions[# i, 0] = "jmp";	
		} else if (instructions[# i, 0] == "jmp") {
			new_instructions[# i, 0] = "nop";	
		} else {
			continue;
		}
		
		day8_reset(new_instructions)
		
		if (day8_terminates(new_instructions)) {
			day8_reset(new_instructions);
			return new_instructions;
		}
		
		day8_reset(new_instructions);
	}
}

// Modified version of day8_run that returns true if the program terminates
function day8_terminates(instructions) {
	var instruction = 0;
	var acc = 0;
	
	while (instruction <= ds_grid_width(instructions) - 1) {
		instructions[# instruction, 2]++;
		
		// Solution to the first part of the problem
		if (instructions[# instruction, 2] > 1) {
			day8_reset(instructions);
			return false;
		}
		
		switch (instructions[# instruction, 0]) {
			case "acc":
				acc += instructions[# instruction, 1];
			case "nop":
				instruction++;
				break;
			case "jmp":
				instruction += instructions[# instruction, 1];
				break;
			default:
				day8_reset(instructions);
				throw("Error in day8_run: invalid instruction \"" + instructions[# instruction, 0] + "\"");
		}
	}
	
	day8_reset(instructions);
	return true;
}

function day8_run(instructions) {
	var instruction = 0;
	var acc = 0;
	
	while (instruction <= ds_grid_width(instructions) - 1) {
		instructions[# instruction, 2]++;
		
		// Solution to the first part of the problem
		if (instructions[# instruction, 2] > 1) {
			day8_reset(instructions);
			return acc;
		}
		
		switch (instructions[# instruction, 0]) {
			case "acc":
				acc += instructions[# instruction, 1];
			case "nop":
				instruction++;
				break;
			case "jmp":
				instruction += instructions[# instruction, 1];
				break;
			default:
				throw("Error in day8_run: invalid instruction \"" + instructions[# instruction, 0] + "\"");
		}
	}
	
	day8_reset(instructions);
	return acc;
}