// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function day7_map() {
	var file = open_file();
	var map = ds_map_create();
	
	while (!file_text_eof(file)) {
		var line = string_replace(file_text_readln(file), "\n", "");
		
		var splits = string_split(string_replace(line, ".", ""), " contain ");
		var bag_key = string_replace(splits[| 0], " bags", "");
		
		map[? bag_key] = ds_map_create();
		
		var contents = string_split(string_replace(splits[| 1], "contain ", ""), ", ");
		
		for (var i = 0; i < ds_list_size(contents); i++) {
			var temp = contents[| i];
			
			if (string_char_at(temp, 1) == " ") {
				temp = string_copy(temp, 2, string_length(temp) - 1);
			}
			
			var space_splits = string_split(temp, " ");
			var key = space_splits[| 1] + " " + space_splits[| 2];
			
			// lol
			try {
				map[? bag_key][? key] = int64(space_splits[| 0]);
			} catch (error) {
				// must be the "no other bags" one
			}
		}
	}
	
	file_text_close(file);
	
	return map;
}

function day7_gold_count(map) {
	var keys = ds_map_keys_to_array(map);
	var count = 0;
	
	for (var i = 0; i < array_length(keys); i++) {
		var nanarchy = day7_anarchy(map, keys[i], []);
		
		if (is_in_array(nanarchy, "shiny gold")) {
			count++;
		}
	}
	
	return count;
}

function day7_part2(map, key) {
	var res = 0;
	
	if (ds_map_size(map[? key]) == 0) {
		return res;	
	}
	
	var keys = ds_map_keys_to_array(map[? key]);
	
	for (var i = 0; i < array_length(keys); i++) {
		res += map[? key][? keys[i]] * (1 + day7_part2(map, keys[i]));
	}
	
	return res;
}

// i don't know
// Returns a list of all the possible bag types this particular bag could
// contain.
//
// @param map map
// @param key key
// @author Villi
// @return Returns a list of all the possible bag types this particular bag could
// contain.
// I would like to thank my parents for getting me to this point and union college for giving me
// the $100 I used to buy GameMaker Studio 2 so I could write this function
//
// All rights reserved Enron Pty Ltd.
function day7_anarchy(map, key, seen) {
	var bag_map = map[? key];
	
	if (ds_map_size(bag_map) == 0) {
		return [];
	}
	
	var keys = ds_map_keys_to_array(bag_map);
	var res = keys;
	
	for (var i = 0; i < array_length(keys); i++) {
		if (is_in_array(seen, keys[i])) {
			continue;
		}
		
		array_push(seen, keys[i]);
		array_join(res, day7_anarchy(map, keys[i], seen));
	}
	
	return res;
}

function day7_print_map(map) {
	show_debug_message(is_undefined(map));
	var str = "{ ";
	var keys = ds_map_keys_to_array(map);
	
	for (var i = 0; i < array_length(keys); i++) {
		str += keys[i] + " : " + ds_map_to_string(map[? keys[i]]);
		
		if (i != array_length(keys) - 1) {
			str += ", ";	
		}
	}
	
	str += " }";
	show_debug_message(str);
}