// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function ds_map_to_string(map) {
	var str = "{ ";
		
	var keys = ds_map_keys_to_array(map);
	for (var i = 0; i < array_length(keys); i++) {
		str += string(keys[i]) + " : " + string(map[? keys[i]]);
		
		if (i != array_length(keys) - 1) {
			str += ", "
		}
	}
	
	str += " }"
	return str;
}