// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

// returns a new array which contains all the values of array1 followed by the values
// of array2
function array_join(array1, array2) {
	for (var i = 0; i < array_length(array2); i++) {
		array_push(array1, array2[i]);
	}
}