// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function open_file() {
	var file;
	file = get_open_filename("text file|*.txt", "");

	if file != "" {
	    var infile = file_text_open_read(file);
	}
	
	return infile;
}