{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 4,
  "bbox_right": 57,
  "bbox_top": 0,
  "bbox_bottom": 60,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"79b4c4b8-d9e4-4d9b-bd50-857445de65d6","path":"sprites/spr_day7_button/spr_day7_button.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"79b4c4b8-d9e4-4d9b-bd50-857445de65d6","path":"sprites/spr_day7_button/spr_day7_button.yy",},"LayerId":{"name":"76eff0ab-15e8-49a6-9eff-640ce150827d","path":"sprites/spr_day7_button/spr_day7_button.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_day7_button","path":"sprites/spr_day7_button/spr_day7_button.yy",},"resourceVersion":"1.0","name":"79b4c4b8-d9e4-4d9b-bd50-857445de65d6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2a5371be-81f0-413f-9e9d-35da9af21950","path":"sprites/spr_day7_button/spr_day7_button.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2a5371be-81f0-413f-9e9d-35da9af21950","path":"sprites/spr_day7_button/spr_day7_button.yy",},"LayerId":{"name":"76eff0ab-15e8-49a6-9eff-640ce150827d","path":"sprites/spr_day7_button/spr_day7_button.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_day7_button","path":"sprites/spr_day7_button/spr_day7_button.yy",},"resourceVersion":"1.0","name":"2a5371be-81f0-413f-9e9d-35da9af21950","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_day7_button","path":"sprites/spr_day7_button/spr_day7_button.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"269b1119-122a-46b9-ad34-ccad48867486","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"79b4c4b8-d9e4-4d9b-bd50-857445de65d6","path":"sprites/spr_day7_button/spr_day7_button.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fb329fdd-1b5a-41ac-a2db-a66ef4984f54","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2a5371be-81f0-413f-9e9d-35da9af21950","path":"sprites/spr_day7_button/spr_day7_button.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 32,
    "yorigin": 32,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_day7_button","path":"sprites/spr_day7_button/spr_day7_button.yy",},
    "resourceVersion": "1.3",
    "name": "spr_day7_button",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"76eff0ab-15e8-49a6-9eff-640ce150827d","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_day7_button",
  "tags": [],
  "resourceType": "GMSprite",
}